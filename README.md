This is my pet Bugsy the Bugdroid, made from pure CSS and happiness.
As such, he is currently allergic to Internet Explorer so keep that away from him.
Not only is Bugsy the epitome of cuteness, he is adventurous, intelligent, and always eager to have some fun.
You can tickle him, spin him and watch him be himself.
If you're lucky, Bugsy might even love you like no other pets can: virtually.
